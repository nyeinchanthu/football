@extends('layouts.app')
@section('content')
	<section id="blog_page">
 <div class="container">
  <div class="row">
   <div class="blog_page_1 clearfix">
   	@foreach($products as $product)
   	<div class="col-sm-3">
	 <div class="blog_page_1i clearfix">
	 <a href="#"> <img height="200px;" src="{{ Voyager::image($product->image) }}" alt="abc" class="iw"></a>
	 <div class="blog_page_1ii clearfix">
	  <h3 class="mgt"><a class="col_1" href="blog_detail.html">{{ $product->name }}</a></h3>
	  <p>{{ Str::limit($product->description, 70) }}</p>
	  <ul>
	   <li><i class="fa fa-calendar-o"></i> {{ $product->created_at }}</li>
	   <li><a class="bold" href="blog_detail.html">PLANNING</a></li>
	  </ul>
	 </div>
	 </div>
	</div>
	@endforeach
   </div>
  </div>
 </div>
</section>
@endsection