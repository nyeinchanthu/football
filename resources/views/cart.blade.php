<!DOCTYPE html>
<html lang="en">
<head>
  <title>Football</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
  <h2>Products Table</h2>
  <p>add to cart testing</p>     
  <div>
    <a href="{{ route('product') }}" class="btn btn-primary" style="float:right; margin-bottom: 10px;">Product Page<small style="font-size: 10px" class="btn btn-intro"></small></a>  
  </div>        
  <table class="table table-bordered">
    <thead>
      <tr>
        <th style="width:15%;">Image</th>
        <th style="width:15%;">Name</th>
        <th style="width:15%;">Price</th>
        <th style="width:10%;">Qty</th>
        <th style="width:15%">Total Price</th>
        <th style="width:20%;">Action</th>
      </tr>
    </thead>
    <tbody>
      @if (session('cart'))
      @foreach(session('cart') as $id=>$details)
      <tr>
        <td>
          <img width="50px;" src="{{ Voyager::image($details['image']) }}">
        </td>
        <td>{{ $details['name'] }}</td>
        <td>{{ $details['price'] }}</td>
        <td>
          <input type="number" value="{{ $details['qty'] }}" name="">
        </td>
        <td>${{ $details['price'] * $details['qty'] }} mmk</td>
        <td class="Action">
          <a href="#" class="btn btn-danger btn-sm deletecart">delete</a>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  <div>
    <a href="{{ route('checkout') }}" class="btn btn-primary" style="float:right; margin-bottom: 10px;">Checkout<small style="font-size: 10px" class="btn btn-intro"></small></a>  
  </div> 
</div>

</body>
</html>

@section('scripts')

@endsection