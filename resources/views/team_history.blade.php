@extends('layouts.app')
@section('content')
<style type="text/css">
img{
	width: 300px;
	height: 250px;
}
</style>
<section id="center" class="clearfix center_blog">
 <div class="container">
  <div class="row">
   <div class="center_blog_1 clearfix">
    <div class="col-sm-6">
	 <div class="center_blog_1l clearfix">
      <h3 class="mgt">Team History</h3>	 
	 </div>
	</div> 
	<div class="col-sm-6">
	 <div class="center_blog_1r text-right clearfix">
	  <ul class="mgt">
	   <li><a href="blog_detail.html">TROPHY</a></li>
	   <li class="border_none"> BOXED THREE COLUMNS</li>
	  </ul>
	 </div>
	</div>
   </div>
  </div>
 </div> 
</section>

<section id="blog_page">
 <div class="container">
  <div class="row">
   <div class="blog_page_1 clearfix">
   	@foreach($teams_history as $team_history)
    <div class="col-sm-3">
    <div class="product_thumb">
	 <div class="blog_page_1i clearfix">
	 <a href="{{ route('team_detail', $team_history->id) }}"> <img src="{{ Voyager::image($team_history->photos) }}" alt="abc" class="iw"></a>
	 <div class="blog_page_1ii clearfix">
	  <h3 class="mgt"><a class="col_1" href="blog_detail.html">{{ $team_history->name }}</a></h3>
	  <p>{{ Str::limit($team_history->description, 70) }}</p>
	  <ul>
	   <li><i class="fa fa-calendar-o"></i>  {{ $team_history->created_at }}</li>
	   <li><a class="bold" href="blog_detail.html">PLANNING</a></li>
	  </ul>
	 </div>
	 </div>
	</div>
	</div>
	@endforeach
  </div>
 </div>
</div>
</section>

@endsection