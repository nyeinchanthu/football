@extends('layouts.app')
@section('content')

<div class="container" style="margin-top: 100px; margin-bottom: 100px;">
	<div class="row">
		<h2 style="margin-bottom: 30px;">My Account</h2>
		<div class="col-md-4">
			<h4>Name</h4>
			<h4>Email</h4>
			<h4>Phone</h4>
			<h4>Address</h4>
		</div>
		<div class="col-md-1">
			<h4>:</h4>
			<h4>:</h4>
			<h4>:</h4>
			<h4>:</h4>
		</div>
		<div class="col-md-4">
			<h4>{{ $my_account->name }}</h4>
			<h4>{{ $my_account->email }}</h4>
			@if($my_account->phone)
			<h4>{{ $my_account->phone }}</h4>
			@else
				<h4>null</h4>
			@endif
			@if($my_account->address)
			<h4>{{ $my_account->address }}</h4>
			@else
				<h4>null</h4>
			@endif
		</div>
	</div>
</div>

@endsection