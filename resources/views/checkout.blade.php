<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<title></title>
</head>
<body>
	<div class="container">
		<div class="row">
			<h3>Checkout Page</h3><br><br>
			<div class="col-md-6">
				<form class="form-horizontal" method="post" action="{{ route('checkout_order') }}">
					{{ csrf_field() }}
					<div class="form-group">
					  <label class="control-label col-sm-2" for="name">Name:</label>
					  <div class="col-sm-10">
					    <input type="text" class="form-control" id="name" placeholder="Enter Your Name" name="name" value="@if(Auth::check()) {{ Auth::user()->name }} @endif">
					  </div>
					</div><br>
					<div class="form-group">
					  <label class="control-label col-sm-2" for="phone">Phone:</label>
					  <div class="col-sm-10">          
					    <input type="text" class="form-control" id="phone" placeholder="Enter Your Phone" name="phone" value="@if (Auth::check()){{Auth::user()->phone}}@endif">
					  </div>
					</div><br>
					<div class="form-group">
					  <label class="control-label col-sm-2" for="address">Address:</label>
					  <div class="col-sm-10">          
					    <input type="text" class="form-control" id="address" placeholder="Enter Your Address" name="address" value="@if (Auth::check()){{Auth::user()->address}}@endif">
					  </div>
					</div><br>
					    @if(!Auth::check())   
                                    <button type="submit" class="btn btn-info  btn-block btn-pill font-size-20 mb-3 py-3">Place order</button>
                                    @endif
				</form>
			</div>
			<div class="col-md-6">
				<form class="js-validate" novalidate="novalidate" method="post" action="{{ route('checkout_order') }}">
                                {{ csrf_field() }}
                                    <!-- Product Content -->
                                    <table class="table">
                                        <thead>
                                            <tr style="background-color: orange;">
                                                <th class="product-name">Product</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	@if(session('cart'))
                                        	@foreach(session('cart') as $id=>$details)
                                                <tr class="cart_item">
                                                    <td>{{ $details['name'] }} x<strong class="product-quantity">{{ $details['qty'] }}</strong></td>
                                                    <td>{{ $details['price'] }}MMK</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Subtotal</th>
                                                <td>
                                                	@if(session('cart'))
                                                	<?php $total=0 ?>
                                                	@foreach(session('cart') as $id=>$details)
                                                		<?php $total += $details['price'] * $details['qty'] ?>
                                                	@endforeach
                                                    <span class="amount">{{ $total }}MMK</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Total</th>
                                                <td>
                                                	@if(session('cart'))
                                                	<?php $total = 0 ?>
                                                	@foreach(session('cart') as $id=>$details)
                                                		<?php $total += $details['price'] * $details['qty'] ?>
                                                	@endforeach
                                                    <strong>
                                                        <span class="amount">{{ $total }}MMK</span>
                                                    </strong>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <!-- End Product Content -->
                                   <!--  <div class="form-group d-flex align-items-center justify-content-between px-3 mb-5">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck10" required
                                                data-msg="Please agree terms and conditions."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                            <label class="form-check-label form-label" for="defaultCheck10">
                                                I have read and agree to the website <a href="#" class="text-blue">terms and conditions </a>
                                                <span class="text-danger">*</span>
                                            </label>
                                        </div>
                                    </div>  -->
                                    @if(Auth::check())   
                                    <button type="submit" class="btn btn-info  btn-block btn-pill font-size-20 mb-3 py-3">Place order</button>
                                    @endif
                                </form>
			</div>
		</div>
	</div>
</body>
</html>