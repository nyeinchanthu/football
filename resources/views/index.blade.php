@extends('layouts.app')
@section('content')
<section id="center" class="clearfix center_home">
 <div class="center_home_m clearfix">
   <div class="carousel slide article-slide" id="article-photo-carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner cont-slider">

    <div class="item active">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football_bunner.jpg') }}">
    </div>
    <div class="item">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football1.jpg') }}">
    </div>
    <div class="item">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football2.jpg') }}">
    </div>
	<div class="item">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football1.jpg') }}">
    </div>
	<div class="item">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football2.jpg') }}">
    </div>
    <div class="item ">
      <img alt="abc" class="iw" src="{{ asset('assets/img/football_bunner.jpg') }}">
    </div>
  </div>
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li class="active" data-slide-to="0" data-target="#article-photo-carousel">
      <img alt="" src="img/1.jpg">
    </li>
    <li class="" data-slide-to="1" data-target="#article-photo-carousel">
      <img alt="" src="img/2.jpg">
    </li>
    <li class="" data-slide-to="2" data-target="#article-photo-carousel">
      <img alt="" src="img/3.jpg">
    </li>
    <li class="" data-slide-to="3" data-target="#article-photo-carousel">
      <img alt="" src="img/4.jpg">
    </li>
	<li class="" data-slide-to="4" data-target="#article-photo-carousel">
      <img alt="" src="img/6.jpg">
    </li>
    <li class="" data-slide-to="5" data-target="#article-photo-carousel">
      <img alt="" src="img/7.jpg">
    </li>
   
	
  </ol>
</div>
   <div class="center_home_mi clearfix">
    <h1>TEAMWORK IS AT THE CORE<br> OF OUR SUCCESS </h1>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit Integer Nec Odio Praesent Libero.</p>
	<h5><a class="button" href="{{ route('blog') }}">READ MORE <i class="fa fa-chevron-right"></i></a></h5>
	<h5><a class="button_1" href="{{ route('blog') }}">SEE MORE <i class="fa fa-chevron-right"></i></a></h5>
   </div>
 </div>
 
</section>


<section id="achieve">
 <div class="container">
  <div class="row">
  	<div class="gallery_h text-center clearfix">
    <div class="col-sm-12">
	 <h2 class="mgt">Cups</h2>
	 <hr>
	</div>
   </div>
   <div class="achieve_1 clearfix">
   	@foreach($blogData as $blog_data)
    <div class="col-sm-4">
	 <div class="achieve_1m clearfix">
	  <div class="achieve_1mi clearfix"><img height="200px;" src="{{ Voyager:: image($blog_data->image) }}" class="iw" alt="abc">
	  </div>
	  <div class="achieve_1mi1 text-center clearfix">
	   <span class="col"><i class="fa fa-star-o"></i></span>
	   <a href="{{ route('details', $blog_data->id) }}">
	   <h3 class="bold col">{{ $blog_data->title }}</h3>
	 </a>
	   <p class="col">{{ Str::limit($blog_data->description, 70) }}</p>
	   <h5><a class="button" href="#">SEE MORE <i class="fa fa-chevron-right"></i></a></h5>
	  </div>
	 </div>
	</div>
	@endforeach
   </div>
  </div>
 </div>
</section>

<section id="players">
 <div class="container">
  <div class="row">
    <div class="gallery_h text-center clearfix">
    <div class="col-sm-12">
	 <h2 class="mgt">Popular Players</h2>
	 <hr>
	</div>
   </div>
    <div class="players_1 clearfix">
    	@foreach($popular_players as $popular_player)
	 <div class="col-sm-3">
	  <div class="players_1i text-center clearfix">
	  	<a href="{{ route('player_detail',$popular_player->id) }}"><img height="250px;" src="{{ Voyager:: image($popular_player->photos) }}" class="iw" alt="abc">
	  	</a>
	   
	   <h3 class="bold">{{ $popular_player->name }}</h3>
	   <h5>DEFENDER</h5>
	  </div>
	 </div>
	 @endforeach
	</div>
  </div>
 </div>
</section>

	<section id="club">
	 <div class="container">
	  <div class="row">
	   <div class="gallery_h text-center clearfix">
	    <div class="col-sm-12">
		 <h2 class="mgt">Products </h2>
		 <hr>
		</div>
	   </div>
	   <div class="achieve_1 clearfix">
	   	@foreach($products_data as $product_data)
		<div class="col-sm-3">
		 <div class="achieve_1m clearfix">
		  <div class="achieve_1mi clearfix">
		   <img height="200px;" width="100px" src="{{ Voyager::image($product_data->image) }}" class="iw" alt="abc">
		  </div>
		  <div class="achieve_1mi1 text-center clearfix">
		   <h5><a href="{{ route('addCart', $product_data->id) }}" class="button_1" href="#">ADD TO CART <i class="fa fa-chevron-right"></i></a></h5>
		  </div>
		 </div>
		 <div class="club_i text-center clearfix">
		  <a href="{{ route('details',$product_data->id) }}"><h4>{{ $product_data->name }}</h4></a>
		  <h1>{{ $product_data->price }}mmk</h1>
		 </div>
		</div>
		@endforeach
	   </div>
	  </div>
	 </div>
	</section>


@endsection