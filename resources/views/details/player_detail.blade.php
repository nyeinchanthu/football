@extends('layouts.app')
@section('content')

<section id="center" class="clearfix center_blog">
 <div class="container">
  <div class="row">
   <div class="center_blog_1 clearfix">
    <div class="col-sm-6">
	 <div class="center_blog_1l clearfix">
      <h3 class="mgt">Player Detail Page Detail</h3>	 
	 </div>
	</div> 
	<div class="col-sm-6">
	 <div class="center_blog_1r text-right clearfix">
	  <ul class="mgt">
	   <li><a href="#">TROPHY</a></li>
	   <li><a href="#">STRATEGY</a></li>
	   <li class="border_none"> BOXED THREE COLUMNS</li>
	  </ul>
	 </div>
	</div>
   </div>
  </div>
 </div> 
</section>

<section id="blog_detail">
 <div class="container">
  <div class="row">
   <div class="blod_d1 clearfix">
    <div class="col-sm-9">
	 <div class="blod_d1lm clearfix">
	  <div class="blod_d1l1 clearfix">
	  	<div>
	  		<img  width="500px;" src="{{ Voyager:: image($player_detail->photos) }}" alt="abc" >
	  	</div>
	   <div class="gallery_h  clearfix">
			 <h3 class="mgt">{{ $player_detail->name }}</h3>
			 <hr>
       </div>
	   <p>{{ $player_detail->description }}</p>
	   <ul>
	    <li><i class="fa fa-calendar-o"></i> {{ $player_detail->created_at }}  </li>
	   </ul>
	  </div>
	 </div>
	</div>
	<div class="col-sm-3">
	 <div class="blod_d1r clearfix">
	  <div class="blod_d1r1 clearfix">
	    <div class="input-group">
			<input type="text" class="form-control" placeholder="Search">
			<span class="input-group-btn">
				<button class="btn btn-primary" type="button">
					<i class="fa fa-search"></i></button>
			</span>
		</div>
		<div class="gallery_h mgt1 clearfix">
			 <h3 class="mgt">About Us</h3>
			 <hr>
       </div>
	   <img src="img/50.jpg" class="iw" alt="abc">
	   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu nisi et turpis mattis tincidunt. Sed lacinia, leo non.</p>
	  </div>
	  <div class="blod_d1r2 clearfix">
	   <div class="gallery_h mgt1 clearfix">
			 <h3>Follow Us</h3>
			 <hr>
       </div>
	   <ul class="social-network social-circle">
                        <li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
       </ul>
	  </div>
	  <div class="blod_d1r3 clearfix">
	   <div class="gallery_h mgt1 clearfix">
			 <h3>Latest Posts</h3>
			 <hr>
       </div>
       @foreach($latest_posts as $latest_post)
	   <div class="blod_d1r3_i clearfix">
	    <div class="col-sm-6 space_left">
		 <div class="blod_d1r3_il">
		  <img height="55px" src="{{ Voyager::image($latest_post->photos) }}" class="iw" alt="abc">
		 </div>
		</div>
		<div class="col-sm-6 space_left">
		 <div class="blod_d1r3_ir">
		  <h5 class="mgt bold"><a href="#">{{ $latest_post->name }}</a></h5>
		  <h6><i class="fa fa-calendar-o"></i>{{ $latest_post->created_at }}</h6>
		 </div>
		</div>
	   </div>
	   @endforeach
	  </div>
	  <div class="blod_d1r5 clearfix">
	   <div class="gallery_h mgt1 clearfix">
			 <h3>Tags</h3>
			 <hr>
       </div>
	   <ul>
	    <li><a href="#">CHAMPIONSHIP</a></li>
        <li><a href="#">CLUB</a></li>
		<li><a href="#">FOOTBALL</a></li>
		<li><a href="#">LEAGUE</a></li>
		<li><a href="#">MATCH</a></li>
		<li><a href="#">PLAYERS</a></li>
		<li><a href="#">RESULTS</a></li>
		<li><a href="#">SOCCER</a></li>
	   </ul>
	   
	  </div>
	 </div>
	</div>
   </div>
  </div>
 </div>
</section>
@endsection