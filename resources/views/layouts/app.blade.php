<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="robots" content="noindex,nofollow" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sprots Channel</title>
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/global.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/index.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}" />
	<link href="{{ asset ('assets/https://fonts.googleapis.com/css?family=Viga&display=swap') }}" rel="stylesheet">
	<script src="{{ asset('assets/js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  </head>
  
<body>
<section id="header">
	<nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
		   <div class="col-sm-12">
		    <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><i class="fa fa-trophy"></i> Sports<span></span></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="tag_menu  active_tab" href="/">HOME</a>
                    </li>
					 
				    <li>
                        <a class="tag_menu" href="{{ route('blog') }}">BLOG</a>
                    </li>
					
					<li>
                        <a class="tag_menu" href="{{ route('player_history') }}">Player History</a>
                    </li>
					
					
					<li>
                        <a class="tag_menu" href="{{ route('team_history') }}">Team History</a>
                    </li>
                    @if(Auth::check())
                    <li class="dropdown">
					  <a class="tag_menu" href="#" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
					  <ul class="dropdown-menu drop_1" role="menu">
						<li><a href="{{ route('my_account') }}">My Account</a></li>
						<li class="nav-item u-header__nav-item">
                            <a class="u-header-collapse__nav-link" href="{{ route('logout') }}" aria-haspopup="true" aria-expanded="false" aria-labelledby="blogSubMenu" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">Logout</a>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
					  </ul>
                    </li>
                    @else
                    <li>
                    	<a class="tag_menu" href="{{ route('login') }}">Login</a>
                    </li>
                    @endif
					<li class="dropdown"><a class="tag_menu" href="#" data-toggle="dropdown"><span class="fa fa-search"></span></a>
							<ul class="dropdown-menu drop_2" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-sm-12">
											<form method="GET" action="{{ route('search') }}" class="navbar-form navbar-left" role="search">
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search" name="keyword" id="keyword">
												<span class="input-group-btn">
													<button class="btn btn-primary" type="button">
														Search</button>
												</span>
											</div>
											</form>
										</div>
									</div>
								</li>
							</ul>
						</li>
					<li class="">
					  <a class="tag_menu" href="{{ route('cart') }}"  role="button" aria-expanded="false"><i class="fa fa-shopping-cart"></i></a>
					  <!-- <ul class="dropdown-menu drop_1" role="menu">
						<li>No products in the cart.</li>
					  </ul> -->
                    </li>
					
					<!-- <li class="dropdown">
					  <a class="tag_menu" href="#" data-toggle="dropdown" role="button" aria-expanded="false">MORE<span class="caret"></span></a>
					  <ul class="dropdown-menu drop_1" role="menu">
						<li><a href="index.html">Home</a></li>
						<li><a href="about.html">About Us</a></li>
						<li><a href="team.html">Team</a></li>
						<li><a href="contact.html">Contact</a></li>
					  </ul>
                    </li> -->
                </ul>
            </div>
		   </div>
        </div>
 
    </nav>
</section>

<main id="content" role="main">
    @yield('content')
</main>

<section id="footer">
 <div class="container">
  <div class="row">
   <div class="footer_1 clearfix">
    <div class="col-sm-3">
	 <div class="footer_1_i clearfix">
	  <h4 class="col bold mgt">About Trophy</h4>
	  <p>Lorem ipsum dolor sit amet, nam ut vero scribentur, mel veritus omnesque ei. Mutat labores mea ex mei.</p>
	  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mi.</p>
	  <h5 class="col"><i class="fa fa-headphones"></i>	Call Us 1-234-567-xyzab</h5>
	 </div>
	</div>
	<div class="col-sm-3">
	 <div class="footer_1_i clearfix">
	  <h4 class="col bold mgt">Latest Posts</h4>
	  <div class="footer_1_ii clearfix"><h5 class="col mgt"><a href="#">Basics First</a> <span>OCT 21, 2016</span></h5></div>
	  <div class="footer_1_ii clearfix"><h5 class="col mgt"><a href="#">Becoming Great</a> <span>OCT 25, 2016</span></h5></div>
	  <div class="footer_1_ii clearfix"><h5 class="col mgt"><a href="#">Better Results</a> <span>OCT 29, 2016</span></h5></div>
	 </div>
	</div>
	
	<div class="col-sm-3">
	 <div class="footer_1_i1 clearfix">
	  <h4 class="col bold mgt">Links</h4>
      <h5><a href="{{ route('about') }}"><i class="fa fa-chevron-right"></i> About Us</a></h5>
	  <h5><a href="{{ route('blog') }}"><i class="fa fa-chevron-right"></i>Blog</a></h5>
	  <h5><a href="{{ route('contact') }}"><i class="fa fa-chevron-right"></i> Contact Us</a></h5>
	  <h5><a href="#"><i class="fa fa-chevron-right"></i> FAQ</a></h5>
	 </div>
	</div>
	
	<div class="col-sm-3">
	 <div class="footer_1_i2 clearfix">
	  <h4 class="col bold mgt">Follow Us</h4>
	  <p>Lorem ipsum dolor sit amet, nam ut vero scribentur, mel veritus omnesque ei. Mutat labores mea ex mei.</p>
      <ul class="social-network social-circle">
                        <li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
       </ul>
	 </div>
	</div>
   </div>
   <div class="footer_2 text-center clearfix">
    <div class="col-sm-12">
	  <p class="mgt col"> © 2013 Your Website Name. All Rights Reserved | Design by <a class="col_1" href="http://www.templateonweb.com">TemplateOnWeb</a></p>
	</div>
   </div>
  </div>
 </div>
</section>

<script>
	$(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '.panel div.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).ready(function () {
    $('.panel-heading span.clickable').click();
    $('.panel div.clickable').click();
});

	</script>

<script>
	  $("#menu-close").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });
	</script>
</body>
 
</html>
