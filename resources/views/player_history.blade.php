@extends('layouts.app')
@section('content')
<style type="text/css">
img{
	height: 200px;
}
</style>
<section id="center" class="clearfix center_blog">
 <div class="container">
  <div class="row">
   <div class="center_blog_1 clearfix">
    <div class="col-sm-6">
	 <div class="center_blog_1l clearfix">
      <h3 class="mgt">Player History</h3>	 
	 </div>
	</div> 
	<div class="col-sm-6">
	 <div class="center_blog_1r text-right clearfix">
	  <ul class="mgt">
	   <li><a href="blog_detail.html">TROPHY</a></li>
	   <li class="border_none"> BOXED THREE COLUMNS</li>
	  </ul>
	 </div>
	</div>
   </div>
  </div>
 </div> 
</section>

<section id="blog_page">
 <div class="container">
  <div class="row">
   <div class="blog_page_1 clearfix">
   	@foreach($players_history as $player_history)
    <div class="col-sm-3">
    <div class="product_thumb">
	 <div class="blog_page_1i clearfix">
	 <a href="{{ route('player_detail',$player_history->id) }}"> <img src="{{ Voyager::image($player_history->photos) }}" alt="abc" class="iw"></a>
	 <div class="blog_page_1ii clearfix">
	  <h3 class="mgt"><a class="col_1" href="blog_detail.html">{{ $player_history->name }}</a></h3>
	  <p>{{ Str::limit($player_history->description, 70) }}</p>
	  <ul>
	   <li><i class="fa fa-calendar-o"></i>  {{ $player_history->created_at }}</li>
	   <li><a class="bold" href="blog_detail.html">PLANNING</a></li>
	  </ul>
	 </div>
	 </div>
	</div>
	</div>
	@endforeach
  </div>
 </div>
</div><br>
<div class="container-fluid">
    <div class="row">
        <div style="text-align: center;" class="col-sm-12 col-md-12 col-lg-12 job_row_body" style="margin-top: 15px;">
            <div class="bg-white-m-left">
                {!! $players_history->links() !!}
            </div>
        </div>
    </div>
</div><br>
</section>

@endsection