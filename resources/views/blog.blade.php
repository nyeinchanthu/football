@extends('layouts.app')
@section('content')

<section id="center" class="clearfix center_blog">
 <div class="container">
  <div class="row">
   <div class="center_blog_1 clearfix">
    <div class="col-sm-6">
	 <div class="center_blog_1l clearfix">
      <h3 class="mgt">Blog Page</h3>	 
	 </div>
	</div> 
	<div class="col-sm-6">
	 <div class="center_blog_1r text-right clearfix">
	  <ul class="mgt">
	   <li><a href="blog_detail.html">TROPHY</a></li>
	   <li class="border_none"> BOXED THREE COLUMNS</li>
	  </ul>
	 </div>
	</div>
   </div>
  </div>
 </div> 
</section>

<section id="blog_page">
 <div class="container">
  <div class="row">
   <div class="blog_page_1 clearfix">
   	@foreach($blogs as $blog)
   	<div class="col-sm-3">
	 <div class="blog_page_1i clearfix">
	 <a href="{{ route('blog_detail', $blog->id) }}"> <img height="200px;" src="{{ Voyager::image($blog->image) }}" alt="abc" class="iw"></a>
	 <div class="blog_page_1ii clearfix">
	  <h3 class="mgt"><a class="col_1" href="blog_detail.html">{{ $blog->title }}</a></h3>
	  <p>{{ Str::limit($blog->description, 70) }}<a>see more</a> </p>
	  <ul>
	   <li><i class="fa fa-calendar-o"></i> {{ $blog->created_at }}</li>
	   <li><a class="bold" href="blog_detail.html">PLANNING</a></li>
	  </ul>
	 </div>
	 </div>
	</div>
	@endforeach
   </div>
  </div>
 </div>
</section>

@endsection