<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamHistory extends Model
{
    protected $table = 'team_history';
    protected $fillable = ['name','description','photos','category_id'];
    use HasFactory;
}
