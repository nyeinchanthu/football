<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PlayerHistory;
use App\Models\Blog;
use App\Models\TeamHistory;

class DetailController extends Controller
{
    public function player_detail($id){
        $player_detail = PlayerHistory::find($id);
        $latest_posts = PlayerHistory::orderBy('player_history.id','desc')
                                    ->limit(3)
                                    ->get();
        return view('details/player_detail',['player_detail'=>$player_detail,'latest_posts'=>$latest_posts]);
    }

    public function blog_detail($id){
        $blog_detail = Blog::find($id);
        $latest_posts = Blog::orderBy('blogs.id','desc')
                                    ->limit(3)
                                    ->get();
        return view('details/blog_detail',['blog_detail'=>$blog_detail,'latest_posts'=>$latest_posts]);
    }

    public function team_detail($id){
        $team_detail = TeamHistory::find($id);
        $latest_posts = TeamHistory::orderBy('team_history.id','desc')
                                    ->limit(3)
                                    ->get();
        return view('details/team_detail',['team_detail'=>$team_detail,'latest_posts'=>$latest_posts]);
    }
}
