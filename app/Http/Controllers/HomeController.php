<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\PlayerHistory;
use App\Models\TeamHistory;
use App\Models\Blog;
use App\Models\Product;
use App\Models\User;

class HomeController extends Controller
{

     public function search(Request $request){

         $keyword = $request->keyword;

        $products = Product::where('name', 'like', '%'.$request->keyword.'%');

        $products = $products->orderBy('created_at', 'desc')
                    ->paginate(20);
        return view('product',['products'=>$products]);

}
    public function index(){
        $blogData =Blog::all();
        $products_data = Product::limit(4)
                                ->get();
        $popular_players = PlayerHistory::limit(4)
                                        ->get();
        return view('/index',['blogData'=>$blogData, 'popular_players'=>$popular_players,'products_data'=>$products_data]);
    }

    public function about(){
        return view('/about_us');
    }

    public function blog(){
        $blogs = Blog::all();
        return view('/blog', compact('blogs'));
    }

    public function player_history(){
        $players_history = PlayerHistory::paginate(4);
        return view('/player_history',compact('players_history'));
    }

    public function team_history(){
        $teams_history = TeamHistory::all();
        return view('/team_history',compact('teams_history'));
    }

    public function product(Request $request){

        $keyword = $request->keyword;
        $products = Product::all();
        if($keyword || $keyword != ''){
            $products = $products->where('name','like','%'.$request->keyword.'%');
        }
        return view('product',['products'=>$products]);
    }

    public function details($id){

         $details = Product::find($id);
         $latest_products = Product::orderBy('products.id','desc')
                                    ->limit(3)
                                    ->get();
         return view('details',['details'=>$details,'latest_products'=>$latest_products]);
    }


    public function my_account(){
        $my_account = User::where('id', Auth::user()->id)->first();
        return view('my_account',['my_account'=>$my_account]);
    }
}
