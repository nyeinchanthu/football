<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends Controller
{
   public function cart(){
    return view('cart');
   }

   public function addCart($id){

    $product = Product::find($id);
    $cart = session()->get('cart');

    if (isset($cart[$id])) {
        $cart[$id]['qty']++;
    } else{
        $cart[$id]= [
                "name"=>$product->name,
                "qty"=>1,
                "price"=>$product->price,
                "image"=>$product->image
        ];
    }

    $cart=session()->put('cart', $cart);
    return redirect()->back();

   }
}
