<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;

class CheckoutController extends Controller
{
    public function checkout(){
        return view('checkout');
    }

    public function checkout_order(Request $request){
        $total = 0;
        if (session('cart')) {
            foreach (session('cart') as $id => $details) {
                $total += $details['price'] * $details['qty'];
                $qty = $details['qty'];
            }
        }
        if (!Auth::user()) {
           
        $vliidatedData = $request->validate([
            "name"=>'required',
            "phone"=>'required',
            "address"=>'required',
        ]);

        $user= new User();
        $user->name=$request['name'];
        $user->phone=$request['phone'];
        $user->address=$request['address'];
        $user->save();
        session()->put('user_id');
        }
        if (!Auth::user()) {
            $order = Order::create([
                'order_date'=>date('Y-m-d'),
                'customer_id'=>$user->id,
                'price'=> $details['price'],
                'total_price'=>$total,
                'qty'=>$qty,
            ]);
        } else{
            $order = Order::create([
                'order_date'=>date('Y-m-d'),
                'customer_id'=>Auth::user()->id,
                'price'=> $details['price'],
                'total_price'=>$total,
                'qty'=>$qty,
            ]);
        }
        Session::forget('cart');
        return redirect()->back();
    }
}
