<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function contact(){
        return view('/contact');
    }
    public function contactstore(Request $request){
        $validateData = $request->validate([
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'text'=> 'required',
        ]);

        $contact = new Contact();
        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->phone = $request['phone'];
        $contact->text = $request['text'];
        $contact-> save();

        return redirect()->back()->with('success', 'Your message has been recorded. thanks!');
    }
}
