<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/',[App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/about_us',[App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('/blog', [App\Http\Controllers\HomeController::class, 'blog'])->name('blog');
Route::get('/player_history', [App\Http\Controllers\HomeController::class, 'player_history'])->name('player_history');
Route::get('/contact',[App\Http\Controllers\ContactController::class, 'contact'])->name('contact');
Route::post('contactstore',[App\Http\Controllers\ContactController::class, 'contactstore'])->name('contactstore');
Route::get('/team_history',[App\Http\Controllers\HomeController::class, 'team_history'])->name('team_history');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//my account
Route::get('my_account', [App\Http\Controllers\HomeController::class, 'my_account'])->name('my_account');

//product route
Route::get('product', [App\Http\Controllers\HomeController::class, 'product'])->name('product');
Route::get('details{id}', [App\Http\Controllers\HomeController::class, 'details'])->name('details');

//cart route
Route::get('cart', [App\Http\Controllers\CartController::class, 'cart'])->name('cart');
Route::get('cart{id}', [App\Http\Controllers\CartController::class, 'addCart'])->name('addCart');

//checkout route
Route::get('checkout', [App\Http\Controllers\CheckoutController::class,'checkout'])->name('checkout');
Route::post('checkout_order', [App\Http\Controllers\CheckoutController::class, 'checkout_order'])->name('checkout_order');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//player detail

Route::get('player_detail{id}', [App\Http\Controllers\DetailController::class, 'player_detail'])->name('player_detail');
Route::get('blog_detail{id}', [App\Http\Controllers\DetailController::class, 'blog_detail'])->name('blog_detail');
Route::get('team_detail{id}', [App\Http\Controllers\DetailController::class, 'team_detail'])->name('team_detail');


//search route
Route::get('search',[App\Http\Controllers\HomeController::class, 'search'])->name('search');